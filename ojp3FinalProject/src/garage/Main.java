package garage;

import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;


/*
 * Robert Wyszecki
 * Technical, Applied Computer Science
 * semester 5
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//brand, model, yearOfProduction, horsepower, color, price, work, numberOfDoors, chassisNumber
		Nissan car1 = new Nissan("Nissan", "GT", 2014, 320, "Silver", 320000, true , 3, 19472);
		Opel car2 = new Opel("Opel", "Astra", 1998, 82, "Green", 5000, true, 3, true);
		Car car3 = new Nissan("Nissan", "Sunny", 2002, 100, "Blue", 2500, false, 5, 19473);
		
		
		//try and catch method
		try{
		car1.work();
		}catch(CarWorkshop e){
			System.out.println(e.getMessage());
		}
		
		
		//using interface
		car2.show_off();
		car3.ride_with_me();
		
		//list
		LinkedList<Vehicle> vehicleList = new LinkedList <Vehicle>();
		
		for(int i = 0;i<3;i++){
			Vehicle car = new Car("Opel", "Astra G", 1998+3*i, 82+40*i, "Green", 1000+2000*i, true, i+1);
			Vehicle motor = new Motorcycle("Kawasaki Ninja", "300", 2012+i, 39, "Red", 24000+i*500, true, 250+20*i);
			
			vehicleList.add(car);
			vehicleList.add(motor);
		}
		
		for(Vehicle vehicle : vehicleList){
			vehicle.showCar();
			vehicle.buyCar();
		}
		
		
		//Mapping
		CarBase serwis = new CarBase("Opel", "Astra G", 1998, 82, "Red", 5000, true, 3, new HashMap<Integer, LinkedList<Integer>>());
		CarBase serwis2 = new CarBase("Volfswagen", "Mk3", 1994, 72, "Violet", 1000, false, 5, new HashMap<Integer, LinkedList<Integer>>());
		CarBase serwis3 = new CarBase("Nissan", "Skyline R34", 2002, 193, "Silver", 30000, true, 3, new HashMap<Integer, LinkedList<Integer>>());
		
		LinkedList<CarBase> carBaseList = new LinkedList<CarBase>();
		carBaseList.add(serwis);
		carBaseList.add(serwis2);
		carBaseList.add(serwis3);
		
		for(CarBase carBase : carBaseList){
			carBase.showCar();
			carBase.ride_with_me();
		}
		
		Car[] auto = new Car[5];
		auto[0] = new Opel("Opel", "Corsa", 1999, 60, "Black", 2000, false, 3, true);
		auto[1] = new Nissan("Nissan", "Nova", 2005, 70, "White", 10000, true, 3, 200010);
		auto[2] = new Opel("Opel", "Vectra", 2000, 90, "Red", 4000, true, 3, true);
		auto[3] = new Nissan("Nissan", "Micra", 1994, 55, "Green", 2500, true, 5, 199992);
		auto[4] = new Opel("Opel", "Tigra", 1998, 90, "Yellow", 6000, true, 3, true);
		
		//Mapping
		Map<Integer, Car> carIndex = new TreeMap<>();
		
		carIndex.put(2000103, auto[0]);
		carIndex.put(1900103, auto[1]);
		carIndex.put(2000003, auto[2]);
		carIndex.put(2000203, auto[3]);
		carIndex.put(800103, auto[4]);
		
		for(Map.Entry<Integer, Car> entry : carIndex.entrySet())
			System.out.println(entry.getKey() + " is index of " + ((Car)entry.getValue()).getBrand() + " " + ((Car)entry.getValue()).getModel()+ " " + ((Car)entry.getValue()).getYOP());

	}
}
