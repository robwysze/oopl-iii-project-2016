package garage;

public class Opel extends Car implements ShowInterface{
	private boolean youOwn;
	
	
	public Opel(String brand, String model, int yearOfProduction, int horsepower, String color, long price, boolean efficiency, int numberOfDoors, boolean youOwn) {
		super(brand, model, yearOfProduction, horsepower, color, price, efficiency, numberOfDoors);
		this.youOwn = youOwn;
		// TODO Auto-generated constructor stub
	}

	public boolean yours()
	{
		return this.youOwn;
	}
	

	@Override
	public void show_off() {
		// TODO Auto-generated method stub
		if(yours() == true)
			System.out.println("Sweet");
		else
			System.out.println("Not everyone need a car");
	}
}
