package garage;

public interface WorkshopInterface {
	public boolean getEfficiency();
	public void setEfficiency(boolean efficiency);
	
	
	default void work() throws CarWorkshop
	{
		if(getEfficiency())
			throw new CarWorkshop();
		else
			setEfficiency(false);
	}
}
