package garage;


import java.util.Random;

public class Vehicle implements BuyInterface{
	private String brand;
	private String model;
	private String color;
	private int yearOfProduction;
	private int horsepower;
	private long price;
	protected boolean efficiency;
	
	public Vehicle(String brand, String model, int yearOfProduction, int horsepower, String color, long price, boolean efficiency)
	{
		this.brand = brand;
		this.model = model;
		this.yearOfProduction = yearOfProduction;
		this.horsepower = horsepower;
		this.color = color;
		this.price = price;
		this.efficiency = efficiency;
	}
	
	public void showCar(){
		System.out.println("Brand: " + this.brand);
		System.out.println("Model: " + this.model);
		System.out.println("Year of production: " + this.yearOfProduction);
		System.out.println("Horsepower: " + this.horsepower);
		System.out.println("Color: " + this.color);
		System.out.println("Starting price: " + this.price + " zl");
		System.out.println("Efficiency: " + this.efficiency);
	}
	
	public String getBrand(){
		return this.brand;
	}
	
	public String getModel(){
		return this.model;
	}
	
	public int getYOP(){
		return this.yearOfProduction;
	}
	
	public int getHorsepower(){
		return this.horsepower;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void setBrand(String brand)
	{
		this.brand = brand;
	}
	
	public void setModel(String model)
	{
		this.model = model;
	}
	
	public void setYOF(int yearOfProduction)
	{
		this.yearOfProduction = yearOfProduction;
	}
	
	public void setHorsePower(int horsepower)
	{
		this.horsepower = horsepower;
	}
	
	public void setColor(String color)
	{
		this.color = color;
	}
	
	public long getPrice()
	{
		return this.price;
	}
	
	public boolean getEfficiency()
	{
		return this.efficiency;
	}

	@Override
	public void buyCar() {
		// TODO Auto-generated method stub
		Random gen = new Random();
		if(getPrice() <=15000 && getPrice() > 0){
			System.out.println("You bought a nice vehicle");
			System.out.println("Final price: " + this.price + " zl");
		}
		else
		{
			try{
			if(gen.nextInt(20)%2 == 0){
				this.price -= 2500; 
				buyCar();
			}
			else{
				System.out.println("Maybe next time, you will buy this car");
				
			}
			}catch(Exception e){
				System.out.println("ERROR");
			}
		}
	}
	
	
	
}
