package garage;

public class Motorcycle extends Vehicle implements RideInterface{

	private float engineCapacity;
	
	public Motorcycle(String brand, String model, int yearOfProduction, int horsepower, String color, long price, boolean efficiency, float engineCapacity) {
		super(brand, model, yearOfProduction, horsepower, color, price, efficiency);
		this.engineCapacity = engineCapacity;
		// TODO Auto-generated constructor stub
	}
	
	public float getEngineCapacity()
	{
		return this.engineCapacity;
	}
	
	public void setEngineCapacity(float engineCapacity)
	{
		this.engineCapacity = engineCapacity;
	}

	@Override
	public void ride_with_me() {
		// TODO Auto-generated method stub
		if(getEngineCapacity()<=50)
			System.out.println("I don't like motorbike");
		else if(!getEfficiency())
			System.out.println("The motorcycle is broken");
		else
			System.out.println("All we had to do was follow the damn train CJ!!");
	}
	
	

}
