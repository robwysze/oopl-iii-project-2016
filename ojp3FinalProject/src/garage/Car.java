package garage;

public class Car extends Vehicle implements RideInterface{
	private int numbersOfDoors;

	public Car(String brand, String model, int yearOfProduction, int horsepower, String color, long price, boolean efficiency, int numberOfDoors) {
		super(brand, model, yearOfProduction, horsepower, color, price, efficiency);
		this.numbersOfDoors = numberOfDoors;
	}
	
	public void setNumberOfDoors(int numbersOfDoors)
	{
		this.numbersOfDoors = numbersOfDoors;
	}
	
	public int getNumberOfDoors()
	{
		return this.numbersOfDoors;
	}

	@Override
	public void ride_with_me() {
		// TODO Auto-generated method stub
		if(getBrand() == "Alfa Romeo")
			System.out.println("It's beautiful car, but often breaks");
		else if(!getEfficiency())
			System.out.println("The car is broken");
		else
			System.out.println("Let's ride");
		
	}
	
	
	
}
