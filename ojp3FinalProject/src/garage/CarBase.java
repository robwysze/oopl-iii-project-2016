package garage;

import java.util.LinkedList;
import java.util.Map;

public class CarBase extends Car{
	
	private Map<Integer, LinkedList<Integer>> indexCar;

	public CarBase(String brand, String model, int yearOfProduction, int horsepower, String color, long price,
			boolean efficiency, int numberOfDoors, Map<Integer, LinkedList<Integer>> indexCar ) {
		super(brand, model, yearOfProduction, horsepower, color, price, efficiency, numberOfDoors);
		this.indexCar = indexCar;
		// TODO Auto-generated constructor stub
	}
	
	public void setIndexCar(Map<Integer, LinkedList<Integer>> indexCar){
		this.indexCar = indexCar;
	}
	
	public Map<Integer, LinkedList<Integer>> getIndexCar(){
		return this.indexCar;
	}


	
	
}
