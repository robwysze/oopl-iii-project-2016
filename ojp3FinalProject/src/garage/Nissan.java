package garage;

public class Nissan extends Car implements WorkshopInterface{
	private long chassisNumber;

	public Nissan(String brand, String model, int yearOfProduction, int horsepower, String color, long price, boolean efficiency,
			int numberOfDoors, int chassisNumber) {
		super(brand, model, yearOfProduction, horsepower, color, price, efficiency, numberOfDoors);
		this.chassisNumber = chassisNumber;
		// TODO Auto-generated constructor stub
	}
	
	public long getChassisNumber()
	{
		return this.chassisNumber;
	}

	@Override
	public void setEfficiency(boolean efficiency) {
		// TODO Auto-generated method stub
		this.efficiency = efficiency;
	
	}
	
	@Override
	public void work() throws CarWorkshop
	{
			if(!getEfficiency())
				throw new CarWorkshop();
			System.out.println("Car working");
	}
	
	public boolean getEfficiency(){
		return this.efficiency;
		
	}


}
