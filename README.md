# README #

Robert Wyszecki
Technical Physics
semester 5

It's simple application, which shows the base vehicle.

How to compile
I've prepared packaged version of this app already. If you want to compile project on your own you can use:
Eclipse IDE to compile project or,
Maven to compile and pack an app to jar archive.

How to run this app
 java -cp ojp3FinalProject-0.0.1-SNAPSHOT.jar garage.Main